# Small Farm Management Simulator
## Overview
This project simulates the management of a small farm where Farmer Tom takes care of various animals, including a cow, two pigs, two goats, and five chickens. The simulation involves feeding the animals, monitoring their food levels, and ensuring their well-being while also managing interactions with Tom's cat, which can play with the animals, affecting their food consumption.

## Features
Farm Visitation: Simulate Tom's visits to the barn to check on and feed the animals.
Feeding Mechanism: Allow Tom to feed specific animals, adjusting their food levels accordingly.
Animal Status Query: Check the health and food level of any specific animal in the barn.
Cat Interaction: The cat randomly plays with an animal, affecting its food level.
Simulation End Conditions: The simulation ends when the user inputs "End" or all animals have died, resulting in Tom's arrest.

## How to Run
Clone this repository to your local machine.
Ensure you have Python installed.
Navigate to the project directory and run python farm_simulator.py in your terminal.
Follow the on-screen instructions to manage the farm.

![Alt text](https://gitlab.com/honeypsabu/gui-farm-game/-/raw/main/Game_GUI.png?ref_type=heads)
