from setuptools import find_packages, setup

setup(
    name="small_farm_task",
    version="1.0.0",
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "farm-terminal = src.farm_terminal:main",
            "farm-gui = src.farm_gui:main",
        ]
    },
    install_requires=[],
    classifiers=[
        "Programming Language :: Python 3.8",
        "Programming Language :: Python 3.9",
    ],
)
