from PyQt5.QtCore import QObject, pyqtSlot

from small_farm_task.model.model import FarmModel


class FarmController(QObject):
    """
    A controller for the farm application.

    The FarmController interacts with the FarmModel to perform actions and retrieve data from the farm.
    """

    def __init__(self, model: FarmModel):
        """
        Initialize the FarmController.

        Args:
            model: The FarmModel instance to interact with.
        """
        super().__init__()
        self._model = model

    def get_animals(self):
        """
        Get the list of animals from the FarmModel.

        Calls the 'get_animals' method of the FarmModel.
        """
        self._model.get_animals()

    @pyqtSlot()
    def reduce_food_10s(self):
        """
        Reduce the food count of animals every 10 seconds.

        Calls the 'reduce_food_10s' method of the FarmModel.
        """
        return self._model.reduce_food_10s()

    @pyqtSlot(object)
    def feed_animal_ctrl(self, animal):
        """
        Feed the specified animal.

        Args:
            animal: The animal to feed.

        Returns:
            The new food value of the animal.

        Calls the 'feed_animal_model' method of the FarmModel.
        """
        return self._model.feed_animal_model(animal)

    @pyqtSlot()
    def play_with_cat_ctrl(self, animal):
        """
        Play with the specified animal.

        Args:
            animal: The animal to play with.

        Returns:
            The new play value of the animal.

        Calls the 'play_with_cat_model' method of the FarmModel.
        """
        return self._model.play_with_cat_model(animal)
