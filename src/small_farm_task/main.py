import sys

from controllers.ctrl import FarmController
from model.model import FarmModel
from PyQt5.QtWidgets import QApplication
from tom_farm.farm_terminal import Farm
from views.view import FarmView


def farm_on_terminal():
    """
    Run the farm simulation on the terminal.

    This function creates an instance of the Farm class and runs the farm simulation on the terminal.
    """
    farm = Farm()
    farm.run_farm()


def farm_on_gui():
    """
    Run the farm simulation using a GUI.

    This function creates an instance of the QApplication, FarmModel, FarmController, and FarmView classes,
    and shows the FarmView GUI to run the farm simulation.
    """
    app = App(sys.argv)
    sys.exit(app.exec_())


class App(QApplication):
    """
    Initialize the application.

    Args:
        sys_argv: The command-line arguments passed to the application.
    """

    def __init__(self, sys_argv):
        super(App, self).__init__(sys_argv)
        self.model = FarmModel()
        self.controller = FarmController(self.model)
        self.view = FarmView(self.controller, self.model)
        self.view.show()


if __name__ == "__main__":
    farm_on_gui()

    # farm_on_terminal()
