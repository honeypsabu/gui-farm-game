from PyQt5.QtCore import QObject, pyqtSignal

from small_farm_task.tom_farm.chicken import Chicken
from small_farm_task.tom_farm.cow import Cow
from small_farm_task.tom_farm.goat import Goat
from small_farm_task.tom_farm.pig import Pig


class FarmModel(QObject):
    """
    A model representing a farm with various animals.
    """

    feed_value_changed = pyqtSignal(object)
    play_with_cat_changed = pyqtSignal(object)
    animals_list = pyqtSignal(list)
    food_count_10s = pyqtSignal(list)

    def __init__(self):
        """
        Initialize the FarmModel.

        The FarmModel contains a list of animals on the farm.
        """
        super().__init__()
        self.animals = [
            Cow("Tony"),
            Pig("Steve"),
            Pig("Bucky"),
            Goat("Nat"),
            Goat("Clint"),
            Chicken("Peter"),
            Chicken("Drax"),
            Chicken("Groot"),
            Chicken("Rocket"),
            Chicken("Mantis"),
        ]

    def get_animals(self):
        """
        Get the list of animals on the farm.

        Emits the 'animals_list' signal with the list of animals.
        """
        self.animals_list.emit(self.animals)

    def reduce_food_10s(self):
        """
        Reduce the food count of animals every 10 seconds.

        Emits the 'food_count_10s' signal with updated information about each animal's food count.
        """
        updated_animals = []
        for animal in self.animals:
            new_food_count = animal.consume_food()
            is_hungry = animal.is_hungry()
            updated_animals.append((animal, new_food_count, is_hungry))
        self.food_count_10s.emit(updated_animals)

    def feed_animal_model(self, animal):
        """
        Feed the specified animal.

        Args:
            animal: The animal to feed.

        Returns:
            The new food value of the animal.

        Emits the 'feed_value_changed' signal with the updated animal.
        """
        if animal in self.animals:
            new_food_value = animal.feed()
            self.feed_value_changed.emit(animal)
            return new_food_value

    def play_with_cat_model(self, animal):
        """
        Play with the specified animal.

        Args:
            animal: The animal to play with.

        Returns:
            The new play value of the animal.

        Emits the 'play_with_cat_changed' signal with the updated animal.
        """
        if animal in self.animals:
            new_play_cat = animal.play()
            self.play_with_cat_changed.emit(animal)
            return new_play_cat
