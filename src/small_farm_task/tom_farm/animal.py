import threading


class Animal:
    """
    The Animal class represents an animal on the farm. It has a name, an amount of food,
    a sound it makes, and a rate at which it consumes food. An animal can be fed to increase
    its food level, played with to reduce its food level, and checked to see if it is hungry.
    """

    def __init__(self, name: str, food: int, sound: str, consumption_rate: int):
        """
        Initializes an instance of the Animal class.

        Args:
            name (str): The name of the animal.
            food (int): The initial amount of food the animal has.
            sound (str): The sound the animal makes.
            consumption_rate (int): The amount of food the animal consumes per cycle.
        """
        self.name = name
        self.food = food
        self.sound = sound
        self.consumption_rate = consumption_rate
        self.total_food = food
        self.lock = threading.Lock()

    def feed(self):
        """
        Feeds the animal by increasing its food level.
        """
        if self.food > 0:
            self.food += self.consumption_rate
            return self.food

    def play(self):
        """
        Reduces the animal's food level by 5 to simulate playing with the animal.
        """
        if self.food > 0:
            new_food_level = self.food - 5
            if new_food_level < 0:
                self.food = 0
            else:
                self.food = new_food_level

    def consume_food(self):
        """
        Reduces the animal's food level by the amount of consumption rate to simulate the animal consuming food.
        """
        with self.lock:
            new_food_level = self.food - self.consumption_rate
            if new_food_level < 0:
                self.food = 0
            else:
                self.food = new_food_level
            print(self.food)

    def is_hungry(self):
        """
        Checks if the animal's food level is below 25% of its total food.

        Returns:
            bool: True if the animal is hungry, False otherwise.
        """
        with self.lock:
            return self.food < 0.25 * self.total_food
