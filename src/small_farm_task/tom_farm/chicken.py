from small_farm_task.tom_farm.animal import Animal


class Chicken(Animal):
    def __init__(self, name: str) -> None:
        """
        Initializes an instance of the Chicken class with a name and specific initial values
        for food, sound, and consumption rate.

        Args:
            name (str): The name of the chicken.
        """
        super().__init__(name, 50, "Cluck...!", 2)
        self.image_path = "C:/Users/honey.sabu/Small Farm Task/src/small_farm_task/resources/images/chick1.jpg"
        if name == "Groot":
            self.image_path = "C:/Users/honey.sabu/Small Farm Task/src/small_farm_task/resources/images/chick5.jpg"
        elif name == "Rocket":
            self.image_path = "C:/Users/honey.sabu/Small Farm Task/src/small_farm_task/resources/images/chick6.jpg"
        elif name == "Peter":
            self.image_path = "C:/Users/honey.sabu/Small Farm Task/src/small_farm_task/resources/images/chick8.jpg"
        elif name == "Drax":
            self.image_path = "C:/Users/honey.sabu/Small Farm Task/src/small_farm_task/resources/images/chick7.jpg"
