from small_farm_task.tom_farm.animal import Animal


class Cow(Animal):
    """
    The Cow class represents a cow on the farm. It is a subclass of the Animal class
    and has a specific initial amount of food, sound, and consumption rate.
    """

    def __init__(self, name: str):
        """
        Initializes an instance of the Cow class with a name and specific initial values
        for food, sound, and consumption rate.

        Args:
            name (str): The name of the cow.
        """
        super().__init__(name, 60, "Moo...!", 5)
        self.image_path = "C:/Users/honey.sabu/Small Farm Task/src/small_farm_task/resources/images/cow.jpg"
