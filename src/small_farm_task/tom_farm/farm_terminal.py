import random
import sys
import threading
import time

from small_farm_task.tom_farm.chicken import Chicken
from small_farm_task.tom_farm.cow import Cow
from small_farm_task.tom_farm.goat import Goat
from small_farm_task.tom_farm.pig import Pig


class Farm:
    def __init__(self):
        self.animals = [
            Cow("Tony"),
            Pig("Steve"),
            Pig("Bucky"),
            Goat("Nat"),
            Goat("Clint"),
            Chicken("Peter"),
            Chicken("Drax"),
            Chicken("Groot"),
            Chicken("Rocket"),
            Chicken("Mantis"),
        ]
        self.timer_thread = threading.Thread(target=self.consume_food)
        self.timer_thread.start()

    def print_status(self) -> None:
        """
        Prints the status of all animals on the farm, including their current food levels and whether they are hungry
        or have starved to death.
        """
        alive_animals = []
        for animal in self.animals:
            print(f"{animal.name} ({animal.__class__.__name__}): food={animal.food}")
            if animal.is_hungry():
                print(f"{animal.sound}, {animal.name} is hungry!")
            if animal.food > 0:
                alive_animals.append(animal)
            else:
                print(f"{animal.name} has starved to death!")
        self.animals = alive_animals
        if len(self.animals) == 0:
            print("All the animals have died! Farmer Tom has been arrested!")
            return

    def consume_food(self) -> None:
        """
        Consumes food for all animals in the list `animals`.
        This function should be run in a separate thread to continuously consume food for the animals.

        Returns:
            None
        """
        while True:
            for animal in self.animals:
                animal.consume_food()
            time.sleep(10)

    def feed_animal(self, animal_name: str) -> None:
        """
        Feeds food for the animal specified in the input. Prints out a message when successful and
        an error message when invalid animal gave is given.
        """
        for animal in self.animals:
            if animal.name == animal_name:
                # feed the same amount as the consumption rate
                amount = animal.consumption_rate
                animal.feed(amount)
                print(f"{animal.name} was fed! {animal.sound} Thanks!")
                break
        else:
            print("Invalid animal name!")

    def play_with_random_animal(self) -> None:
        """
        A random animal is chosen to play with the cat.
        """
        animal = random.choice(self.animals)
        animal.play()
        print(f"The cat played with {animal.name} ({animal.__class__.__name__})!")

    def query_animal(self, animal_name: str) -> None:
        """
        Queries for the animal specified in the input and prints out the amount of food left.

        Returns:
            None
        """
        for animal in self.animals:
            if animal.name == animal_name:
                print(
                    f"{animal.name} ({animal.__class__.__name__}): food={animal.food}, sound={animal.sound}"
                )
                break
        else:
            print("Invalid animal name!")

    def run_farm(self) -> None:
        """
        This portion runs an interactive small farm game on the terminal.

        Returns:
            None
        """
        while True:
            command = input("Enter a command (visit/feed/play/query/end): ")
            if command == "visit":
                self.print_status()
            elif command == "feed":
                animal_name = input(
                    "Enter the name of the animal you want to feed ("
                    "Tony/Steve/Bucky/Nat/Clint/Peter/Drax/Groot/Rocket/Mantis): "
                )
                self.feed_animal(animal_name)
            elif command == "play":
                self.play_with_random_animal()
            elif command == "query":
                animal_name = input(
                    "Enter the name of the animal you want to query ("
                    "Tony/Steve/Bucky/Nat/Clint/Peter/Drax/Groot/Rocket/Mantis): "
                )
                self.query_animal(animal_name)
            elif command == "end":
                sys.exit("Exiting program...")
            else:
                print("Invalid command!")
