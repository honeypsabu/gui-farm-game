from small_farm_task.tom_farm.animal import Animal


class Goat(Animal):
    def __init__(self, name: str) -> None:
        """
        Initializes an instance of the Goat class with a name and specific initial values
        for food, sound, and consumption rate.

        Args:
            name (str): The name of the goat.
        """
        super().__init__(name, 40, "Maah...!", 3)
        self.image_path = "C:/Users/honey.sabu/Small Farm Task/src/small_farm_task/resources/images/goat1.jpg"
        if name == "Nat":
            self.image_path = "C:/Users/honey.sabu/Small Farm Task/src/small_farm_task/resources/images/goat2.jpg"
