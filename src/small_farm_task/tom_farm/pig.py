from small_farm_task.tom_farm.animal import Animal


class Pig(Animal):
    def __init__(self, name: str) -> None:
        """
        Initializes an instance of the Pig class with a name and specific initial values
        for food, sound, and consumption rate.

        Args:
            name (str): The name of the pig.
        """
        super().__init__(name, 50, "Oink...!", 4)
        self.image_path = "C:/Users/honey.sabu/Small Farm Task/src/small_farm_task/resources/images/pig1.jpg"
        if name == "Steve":
            self.image_path = "C:/Users/honey.sabu/Small Farm Task/src/small_farm_task/resources/images/pig2.jpg"
