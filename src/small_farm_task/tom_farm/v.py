import random
import sys

from PyQt5.QtCore import Qt, QTimer, pyqtSlot
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import (QApplication, QHBoxLayout, QLabel, QPushButton,
                             QSizePolicy, QTextEdit, QVBoxLayout, QWidget)

from small_farm_task.controllers.ctrl import FarmController
from small_farm_task.model.model import FarmModel


class AnimalView(QWidget):
    def __init__(self, controller: FarmController, model: FarmModel):
        super().__init__()

        self.edit_widgets = {}
        self.initial_food_counts = {}
        self.view_layout = None
        self._controller = controller
        self._model = model
        # Create layout for animal views
        self.animal_layout = QHBoxLayout()
        self.animal_layout.setAlignment(Qt.AlignCenter)

        self.setFixedSize(1800, 1000)  # Set the window size to 800x600 pixels

        self.welcome_layout = QVBoxLayout()

        # Create label for animal views
        self.animal_label = QLabel("Welcome to the Farm!")
        self.animal_label.setAlignment(Qt.AlignCenter)
        self.pixmap = QPixmap("/small_farm_task/resources/images/farm.jpg")
        self.pixmap = self.pixmap.scaledToHeight(200, Qt.SmoothTransformation)
        self.animal_label.setPixmap(self.pixmap)
        self.welcome_layout.addWidget(self.animal_label)

        # Create layout for "End" and "Play" buttons
        self.game_button_layout = QVBoxLayout()
        self.game_button_layout.setAlignment(Qt.AlignCenter)

        # Create "End" and "Play" buttons
        self.end_button = QPushButton("End")
        self.end_button.clicked.connect(self.end_game)
        self.play_button = QPushButton("Play with cat")
        self.play_button.clicked.connect(self.play_with_cat)

        self._model.play_with_cat_changed.connect(
            lambda animal: self.update_play_cat(animal)
        )

        # Create "Restart" button
        self.restart_button = QPushButton("Restart")
        self.restart_button.clicked.connect(self.restart_game)

        # Create label for animal views
        self.cat_label = QLabel("Play with the cat")
        self.cat_label.setAlignment(Qt.AlignCenter)
        self.pixmap = QPixmap("/small_farm_task/resources/images/cat.jpg")
        self.pixmap = self.pixmap.scaledToHeight(200, Qt.SmoothTransformation)
        self.cat_label.setPixmap(self.pixmap)

        # Create a label for status display
        self.status_label = QLabel("Status: Waiting for the game to start...")
        self.status_label.setFixedSize(300, 100)

        # Add "End" and "Play" buttons to layout
        self.game_button_layout.addWidget(self.cat_label, alignment=Qt.AlignCenter)
        self.game_button_layout.addWidget(self.play_button)
        self.game_button_layout.addWidget(self.status_label)
        self.game_button_layout.addWidget(self.restart_button)
        self.game_button_layout.addWidget(self.end_button)

        # Create main layout and add sub-layouts
        self.main_layout = QVBoxLayout()
        self.main_layout.addLayout(self.welcome_layout)
        self.main_layout.addLayout(self.animal_layout)
        self.main_layout.addLayout(self.game_button_layout)

        self.setLayout(self.main_layout)

        self._model.animals_list.connect(lambda animals: self.init_animals(animals))
        # self.play_button.clicked.connect(lambda : self.play_with_cat)

        self._controller.get_animals()

    def init_animals(self, animals):
        self.animals = animals
        # Create views for each animal
        for animal in self.animals:
            # Create layout for view
            self.initial_food_counts[animal] = animal.food
            self.view_layout = QVBoxLayout()
            self.view_layout.setAlignment(Qt.AlignCenter)

            # Create label for animal image
            label = QLabel()
            label.setAlignment(Qt.AlignCenter)
            label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
            pixmap = QPixmap(animal.image_path)
            pixmap = pixmap.scaledToHeight(170, Qt.SmoothTransformation)
            label.setPixmap(pixmap)
            self.view_layout.addWidget(label)

            self.edit = QTextEdit()
            self.edit.setAlignment(Qt.AlignCenter)
            self.edit.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
            self.view_layout.addWidget(self.edit)
            self.edit_widgets[animal] = self.edit

            # Set initial food count based on animal type
            self.edit.setText(str(animal.food))

            # Create button for animal
            self.button = QPushButton("Feed " + animal.name)
            self.button.clicked.connect(
                lambda _, animal=animal: self.feed_animal(animal)
            )
            self.view_layout.addWidget(self.button)
            self.button.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)

            # Add view layout to animal layout
            self.animal_layout.addLayout(self.view_layout)
            self._model.feed_value_changed.connect(
                lambda animal: self.update_feed_animal(animal)
            )

            label.setFixedSize(label.sizeHint())

    def feed_animal(self, animal):
        self._controller.feed_animal_ctrl(animal)

    @pyqtSlot()
    def update_feed_animal(self, animal):
        edit = self.edit_widgets[animal]
        edit.setText(str(animal.food))

    def play_with_cat(self):
        animal = random.choice(self.animals)
        self._controller.play_with_cat_ctrl(animal)

    @pyqtSlot()
    def update_play_cat(self, animal):
        edit = self.edit_widgets[animal]
        edit.setText(f"<font color='purple'>The cat played with {animal.name}!")
        QTimer.singleShot(1000, lambda: edit.setText(str(animal.food)))

    def end_game(self):
        QApplication.quit()

    def restart_game(self):
        for animal, initial_food in self.initial_food_counts.items():
            animal.food = initial_food
            self.edit_widgets[animal].setText(str(initial_food))


if __name__ == "__main__":
    model = FarmModel()
    controller = FarmController(model)
    app = QApplication(sys.argv)
    view = AnimalView(controller, model)
    view.show()
    sys.exit(app.exec_())
