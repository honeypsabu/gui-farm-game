import threading
import time

from PyQt5.QtCore import QObject, pyqtSignal

from small_farm_task.controllers.ctrl import FarmController


class Timer(QObject):
    """
    A timer class that emits a signal at regular intervals.

    The Timer class provides a way to start, stop, and reset a timer, and emits a signal at each interval.
    """

    timer_signal = pyqtSignal()

    def __init__(self, interval):
        """
        Initialize the Timer.

        Args:
            interval: The interval in seconds between each timer signal.
        """
        super().__init__()
        self.elapsed_time = None
        self._controller = FarmController
        self.interval = interval
        self.is_running = False
        self.thread = None

    def start(self):
        """
        Start the timer.

        If the timer is not already running, starts a new thread and calls the 'timer_loop' method.
        """
        if not self.is_running:
            self.is_running = True
            self.thread = threading.Thread(target=self.timer_loop)
            self.thread.start()

    def stop(self):
        """
        Stop the timer.

        Sets the 'is_running' flag to False and joins the timer thread.
        """
        self.is_running = False
        if self.thread is not None:
            self.thread.join()
            self.thread = None

    def reset(self):
        """
        Reset the timer.

        Resets the elapsed time to 0 and stops the timer.
        """
        self.elapsed_time = 0
        self.stop()

    def timer_loop(self):
        """
        The main loop of the timer.

        Emits the 'timer_signal' signal at regular intervals as long as the timer is running.
        """
        while self.is_running:
            self.timer_signal.emit()
            time.sleep(self.interval)
