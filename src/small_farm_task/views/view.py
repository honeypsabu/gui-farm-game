import random

from PyQt5.QtCore import Qt, QTimer, pyqtSlot
from PyQt5.QtGui import QFont, QPixmap
from PyQt5.QtWidgets import (QApplication, QHBoxLayout, QLabel, QMessageBox,
                             QPushButton, QSizePolicy, QTextEdit, QVBoxLayout,
                             QWidget)

from small_farm_task.controllers.ctrl import FarmController
from small_farm_task.model.model import FarmModel
from small_farm_task.views.timer import Timer


class FarmView(QWidget):
    def __init__(self, controller: FarmController, model: FarmModel):
        """
        Initialize the FarmView.

        Args:
            controller: An instance of the FarmController class.
            model: An instance of the FarmModel class.
        """
        super().__init__()

        self.edit_widgets = {}
        self.initial_food_counts = {}
        self.view_layout = None
        self._controller = controller
        self._model = model

        # Create an instance of Timer with an interval of 10 second
        self.timer = Timer(interval=10)
        self.timer.timer_signal.connect(self.update_timer_signal)

        # Create layout for animal views
        self.animal_layout = QHBoxLayout()
        self.animal_layout.setAlignment(Qt.AlignCenter)

        self.setFixedSize(1800, 1000)

        self.welcome_layout = QVBoxLayout()

        self.font = QFont()
        self.font.setBold(True)
        self.font.setPointSize(12)

        # Create label for animal views
        self.animal_label = QLabel("Welcome to the Farm!")
        self.animal_label.setAlignment(Qt.AlignCenter)
        self.pixmap = QPixmap(
            "C:/Users/honey.sabu/Small Farm Task/src/small_farm_task/resources/images/farm.jpg"
        )
        self.pixmap = self.pixmap.scaledToHeight(200, Qt.SmoothTransformation)
        self.animal_label.setPixmap(self.pixmap)
        self.welcome_layout.addWidget(self.animal_label)

        # Create layout for "End" and "Play" buttons
        self.game_button_layout = QVBoxLayout()
        self.game_button_layout.setAlignment(Qt.AlignCenter)

        # Create "End" and "Play" buttons
        self.end_button = QPushButton("End")
        self.end_button.setFont(self.font)
        self.end_button.clicked.connect(self.end_game)
        self.play_button = QPushButton("Play with cat")
        self.play_button.setFont(self.font)
        self.play_button.clicked.connect(self.play_with_cat)

        self._model.play_with_cat_changed.connect(
            lambda animal: self.update_play_cat(animal)
        )

        # Create "Start" button
        self.start_button = QPushButton("Start Game")
        self.start_button.setFont(self.font)
        self.start_button.clicked.connect(self.start_timer)

        # Create "Restart" button
        self.restart_button = QPushButton("Restart")
        self.restart_button.setFont(self.font)
        self.restart_button.clicked.connect(self.restart_game)

        # Create label for animal views
        self.cat_label = QLabel("Play with the cat")
        self.cat_label.setAlignment(Qt.AlignCenter)
        self.pixmap = QPixmap(
            "C:/Users/honey.sabu/Small Farm Task/src/small_farm_task/resources/images/cat.jpg"
        )
        self.pixmap = self.pixmap.scaledToHeight(200, Qt.SmoothTransformation)
        self.cat_label.setPixmap(self.pixmap)

        # Create a label for status display
        self.status_label = QLabel("Status: Waiting for the game to start...")
        self.status_label.setWordWrap(True)
        self.status_label.setAlignment(Qt.AlignCenter)
        self.status_label.setFont(self.font)
        self.status_label.setFixedSize(300, 100)

        # Add "End" and "Play" buttons to layout
        self.game_button_layout.addWidget(self.cat_label, alignment=Qt.AlignCenter)
        self.game_button_layout.addWidget(self.play_button)
        self.game_button_layout.addWidget(self.status_label)
        self.game_button_layout.addWidget(self.start_button)
        self.game_button_layout.addWidget(self.restart_button)
        self.game_button_layout.addWidget(self.end_button)

        # Create main layout and add sub-layouts
        self.main_layout = QVBoxLayout()
        self.main_layout.addLayout(self.welcome_layout)
        self.main_layout.addLayout(self.animal_layout)
        self.main_layout.addLayout(self.game_button_layout)

        self.setLayout(self.main_layout)

        self._model.animals_list.connect(lambda animals: self.init_animals(animals))
        self._model.food_count_10s.connect(
            lambda animals: self.update_food_10s(animals)
        )

        self._controller.get_animals()

    def init_animals(self, animals):
        """
        Initialize the animal views.

        Args:
            animals: A list of animal objects.
        """
        self.animals = animals
        # Create views for each animal
        for animal in self.animals:
            # Create layout for view
            self.initial_food_counts[animal] = animal.food
            self.view_layout = QVBoxLayout()
            self.view_layout.setAlignment(Qt.AlignCenter)

            # Create label for animal image
            label = QLabel()
            label.setAlignment(Qt.AlignCenter)
            label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
            pixmap = QPixmap(animal.image_path)
            pixmap = pixmap.scaledToHeight(170, Qt.SmoothTransformation)
            label.setPixmap(pixmap)
            self.view_layout.addWidget(label)

            self.edit = QTextEdit()
            self.edit.setAlignment(Qt.AlignCenter)
            self.edit.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
            self.edit.setFont(self.font)
            self.view_layout.addWidget(self.edit)
            self.edit_widgets[animal] = self.edit

            # Set initial food count based on animal type
            self.edit.setText(str(animal.food))

            # Create button for animal
            self.button = QPushButton("Feed " + animal.name)
            self.button.setFont(self.font)
            self.button.clicked.connect(
                lambda _, animal=animal: self.feed_animal(animal)
            )
            self.view_layout.addWidget(self.button)
            self.button.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)

            # Add view layout to animal layout
            self.animal_layout.addLayout(self.view_layout)
            self._model.feed_value_changed.connect(
                lambda animal: self.update_feed_animal(animal)
            )

            label.setFixedSize(label.sizeHint())

    def feed_animal(self, animal):
        """
        Feed the specified animal.

        Args:
            animal: The animal to be fed.
        """
        self._controller.feed_animal_ctrl(animal)

    @pyqtSlot()
    def update_feed_animal(self, animal):
        """
        Update the feed count of the specified animal.

        Args:
            animal: The animal to update the feed count for.
        """
        edit = self.edit_widgets[animal]
        edit.setText(str(animal.food))

    @pyqtSlot(list)
    def update_food_10s(self, animals):
        """
        Update the food count every 10 seconds.

        Args:
            animals: A list of tuples containing the animal, new food count, and hunger status.
        """
        all_dead = True

        for animal, new_food_count, is_hungry in animals:
            self.timer.start()
            edit = self.edit_widgets[animal]

            if animal.food == 0:
                edit.setText(f"<font color='blue'>{animal.name} is dead!")
                self.status_label.setText(f"<font color='blue'>{animal.name} is dead!")
            else:
                edit.setText(str(animal.food))
                all_dead = False

            if is_hungry:
                if animal.food > 0:
                    edit.setText(
                        f"<font color='teal'>{animal.food} \n {animal.sound} {animal.name} is hungry!"
                    )

        if all_dead:
            # Display a status message
            self.status_label.setText(
                f"<font color='red'>All the animals are dead. \nTom is arrested by the Police!"
            )
            # All animals are dead, display a message box
            self.msg_box = QMessageBox(self)
            self.msg_box.setFont(self.font)
            self.msg_box.setWindowTitle("Game Over")
            self.msg_box.setText(
                f"<font color='red'>All the animals are dead. \nTom is arrested by the Police!"
            )
            self.msg_box.setStandardButtons(QMessageBox.Ok)
            self.msg_box.buttonClicked.connect(self.msg_box.close)
            self.msg_box.exec_()
            self.timer.stop()

    def play_with_cat(self):
        """
        Cat plays with a random animal.
        """
        alive_animals = [animal for animal in self.animals if animal.food > 0]
        if alive_animals:
            animal = random.choice(alive_animals)
            self._controller.play_with_cat_ctrl(animal)

    @pyqtSlot()
    def update_play_cat(self, animal):
        """
        Update the UI after playing with the cat.

        Args:
            animal: The animal that played with the cat.
        """
        edit = self.edit_widgets[animal]
        edit.setText(f"<font color='purple'>The cat played with {animal.name}!")
        QTimer.singleShot(1000, lambda: edit.setText(str(animal.food)))

    def end_game(self):
        """
        End the game.
        """
        # Show a pop-up message asking to confirm game ending
        msg_box = QMessageBox(self)
        msg_box.setFont(self.font)
        msg_box.setWindowTitle("End Game")
        msg_box.setText(f"<font color='green'>Do you want to end the game?")
        msg_box.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        button = msg_box.exec_()

        if button == QMessageBox.Yes:
            # Game ending confirmed, quit the application
            QApplication.quit()

    def restart_game(self):
        """
        Restart the game and update the game status label.
        """
        for animal, initial_food in self.initial_food_counts.items():
            self.timer.reset()
            animal.food = initial_food
            self.edit_widgets[animal].setText(str(initial_food))
            self.status_label.setText("Status: Waiting for the game to start...")

    @pyqtSlot()
    def update_timer_signal(self):
        """
        Slot function for updating the timer signal.
        """
        print("timer started")
        self.reduce_food_10s()

    def start_timer(self):
        """
        Start the timer and update the game status label.
        """
        self.timer.start()
        self.status_label.setText(
            "<font color='olive'>The game has started. Good Luck!"
        )

    def reduce_food_10s(self):
        """
        Reduce the food count of all animals every 10 seconds.
        """
        self._controller.reduce_food_10s()

    def stop_timer(self):
        """
        Stop the timer.
        """
        self.timer.stop()
